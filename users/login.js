'use strict';


const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const Bcrypt = require('./bcrypt');
const Lodash = require('lodash');


module.exports.login = (event, context, callback) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-users`,
    //KeyConditionExpression: "#email = :email",
    //ExpressionAttributeNames:{
      //"#email": "email"
    //},
    ExpressionAttributeValues: {
      ":email": data.email
    },
     FilterExpression: "contains (email, :email)",
  };

  dynamoDb.scan(params, (error, result) => {
    console.log('asd', JSON.stringify(result, null, 2));
    const listUsers = result && result.Items ? result.Items : [];
    const objUser = Lodash.get(listUsers, [0], {});
    const t = Bcrypt.isValidPassword(data.password, objUser.userpassword)
    if (t) {
      const response = { statusCode: 200, body: JSON.stringify(objUser) };
      callback(null, response);
    } else {
      const response = { statusCode: 401, body: JSON.stringify({}) };
      callback(null, response);
    }
  });
};