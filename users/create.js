'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');
const bcrypt = require('./bcrypt');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.create = (event, context, callback) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-users`,
    Item: {
      id: uuid.v1(),
      firstname: data.firstname,
      lastname: data.lastname,
      streetname: data.streetname,
      streetnumber: data.streetnumber,
      neighborhood: data.neighborhood,
      city: data.city,
      employee_type: data.employee_type,
      email: data.email,
      userpassword: bcrypt.hashPassword(data.password)
    },
  };
  

  dynamoDb.put(params, (error) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t create the crew member.',
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(params.Item),
    };
    callback(null, response);
  });
};
