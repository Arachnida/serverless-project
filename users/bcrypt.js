"use strict";

module.exports = { isValidPassword, hashPassword };

let bcrypt = require('bcryptjs');

function isValidPassword(strInputPassword, strUserPassword) {
    return bcrypt.compareSync(strInputPassword, strUserPassword);
}

function hashPassword(strInputPassword) {
    var salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(strInputPassword, salt);
}