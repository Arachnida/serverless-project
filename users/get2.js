'use strict';

const AWS = require('aws-sdk');
var urlencode = require('urlencode');

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: `${process.env.DYNAMODB_TABLE}-users`,
};

module.exports.get = (event, context, callback) => {
  dynamoDb.scan(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t fetch the user.',
      });
      return;
    }
    const data = result && result.Items ? result.Items : [];
    const t = urlencode.decode(event.pathParameters.city, 'gbk');
    const _data = data.filter((user) => {
      return user.city == t
    }
    );
    const response = {
      statusCode: 200,
      body: JSON.stringify(_data)
    };
    callback(null, response);
  });
};
