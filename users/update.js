'use strict';

const AWS = require('aws-sdk'); 
const bcrypt = require('./bcrypt');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.update = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-users`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':firstname': data.firstname,
      ':lastname': data.lastname,
      ':streetname': data.streetname,
      ':streetnumber': data.streetnumber,
      ':neighborhood': data.neighborhood,
      ':city': data.city,
      ':employee_type': data.employee_type,
      ':email': data.email,
      ':userpassword': bcrypt.hashPassword(data.password)
    },
    UpdateExpression: 'SET firstname = :firstname, lastname = :lastname, streetname = :streetname, streetnumber = :streetnumber, neighborhood = :neighborhood, city = :city, employee_type = :employee_type, email = :email, userpassword = :userpassword',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t fetch the crew member.',
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};
