'use strict';

const AWS = require('aws-sdk'); 

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.update = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-route`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':routename': data.routename,
      ':pickupdate': data.pickupdate,
      ':crew': data.crew,
      ':customer': data.customer
    },
    UpdateExpression: 'SET routename = :routename, pickupdate = :pickupdate, crew = :crew, customer = :customer',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t fetch the route.',
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};
