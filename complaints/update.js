'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.update = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-complaints`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':customer': data.customer,
      ':problem': data.problem,
    },
    UpdateExpression: 'customer = :customer, problem = :problem',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t fetch the complaints.',
      });
      return;
    }
    
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
    
  });
};
