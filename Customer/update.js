'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.update = (event, context, callback) => {
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  const params = {
    TableName: `${process.env.DYNAMODB_TABLE}-customer`,
    Key: {
      id: event.pathParameters.id,
    },
    ExpressionAttributeValues: {
      ':user': data.user,
      ':latitude': data.latitude,
      ':longitude': data.longitude

    },
    ExpressionAttributeNames: {
      "#user": "user"
    },
    UpdateExpression: 'SET #user = :user, latitude = :latitude, longitude = :longitude',
    ReturnValues: 'ALL_NEW',
  };

  dynamoDb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t fetch the customer.',
      });
      return;
    }
    
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
    
  });
};
